import React, { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const ExampleComponent = ({someText}) => {
   return (
      <Fragment>
         {someText}
      </Fragment>
   )
}

ExampleComponent.propTypes = {
   someText: PropTypes.string.isRequired,
}

export default ExampleComponent
