import React, { useState, useEffect } from 'react';

interface Props {
   someText: string,
   aFunc?: (thing:string) => void,
}

const TSExampleComponent: React.FC<Props> = ({someText}) => {
   return (
      <div>
         {someText}
      </div>
   )
}

export default TSExampleComponent
