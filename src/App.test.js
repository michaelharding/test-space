import { render, screen } from '@testing-library/react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import App from './App';

configure({ adapter: new Adapter() })

describe('App', () => {
  // test('renders learn react link', () => {
  //   render(<App />);
  //   const linkElement = screen.getByText(/learn react/i);
  //   expect(linkElement).toBeInTheDocument();
  // });

  test('Using Enzyme', () => {
    const wrapper = mount(<App />);
    console.debug(wrapper.debug());
    expect(wrapper.find('ExampleComponent').text()).toContain('This is Some Text');
  });
});
