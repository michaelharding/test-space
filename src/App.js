import ExampleComponent from './Components/ExampleComponent';
import './App.css';

function App() {
  return (
    <div className="App">
      <ExampleComponent someText="This is Some Text" />
    </div>
  );
}

export default App;
